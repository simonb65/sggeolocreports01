﻿using System;


namespace SgGeolocReports
{
    public struct GeoCoordinate
    {
        public GeoCoordinate(double latitude, double longitude, int ordinal)
        {
            Latitude = latitude;
            Longitude = longitude;
            Ordinal = ordinal;
        }
        public double Latitude { get; }
        public double Longitude { get; }
        public int Ordinal { get; }
    }

    public abstract class GeoFence
    {
        protected GeoFence(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public int Id { get;  }
        public string Name { get; }

        public abstract bool ContainsPoint(double latitude, double longitude);
    }


    public class PolygonGeoFence : GeoFence
    {
        public PolygonGeoFence(int id, string name, GeoCoordinate[] geoCoordinates)
            : base(id, name)
        {
            GeoGeoCoordinates = geoCoordinates;
        }

        public GeoCoordinate[] GeoGeoCoordinates { get; }

        public override bool ContainsPoint(double latitude, double longitude)
        {
            var j = GeoGeoCoordinates.Length - 1;
            var pointStatus = false;
            for (var i = 0; i < GeoGeoCoordinates.Length; i++)
            {
                if (GeoGeoCoordinates[i].Latitude < latitude && GeoGeoCoordinates[j].Latitude >= latitude ||
                    GeoGeoCoordinates[j].Latitude < latitude && GeoGeoCoordinates[i].Latitude >= latitude)
                {
                    if (GeoGeoCoordinates[i].Longitude + (latitude - GeoGeoCoordinates[i].Latitude) /
                        (GeoGeoCoordinates[j].Latitude - GeoGeoCoordinates[i].Latitude) * (GeoGeoCoordinates[j].Longitude - GeoGeoCoordinates[i].Longitude) < longitude)
                    {
                        pointStatus = !pointStatus;
                    }
                }
                j = i;
            }
            return pointStatus;
        }
    }

    public class CircleGeoFence : GeoFence
    {
        public CircleGeoFence(int id, string name, GeoCoordinate centre, double radius) 
            : base(id, name)
        {
            Centre = centre;
            Radius = radius;
        }
         
        public GeoCoordinate Centre { get; }
        public double Radius { get; }

        // The distance between the two coordinates, in meters.
        private double GetDistanceTo(double latitude, double longitude)
        {
            if (double.IsNaN(latitude) || double.IsNaN(longitude))
                throw new ArgumentException("Argument latitude or longitude is not a number");
            
            var d1 = Centre.Latitude * (Math.PI / 180.0);
            var num1 = Centre.Longitude * (Math.PI / 180.0);
            var d2 = latitude * (Math.PI / 180.0);
            var num2 = longitude * (Math.PI / 180.0) - num1;
            var d3 = Math.Pow(Math.Sin((d2 - d1) / 2.0), 2.0) +
                     Math.Cos(d1) * Math.Cos(d2) * Math.Pow(Math.Sin(num2 / 2.0), 2.0);

            return 6376500.0 * (2.0 * Math.Atan2(Math.Sqrt(d3), Math.Sqrt(1.0 - d3)));
        }

        public override bool ContainsPoint(double latitude, double longitude)
        {
            var dist = GetDistanceTo(latitude, longitude);
            return dist < Radius;
        }
    }
}
