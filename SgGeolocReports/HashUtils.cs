﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace SgGeolocReports
{
    public static class HashUtils
    {
        public static string ByteArrayToString(byte[] input)
        {
            var sb = new StringBuilder(input.Length * 2);

            foreach (var b in input)
            {
                sb.Append(b.ToString("X2"));
            }

            return sb.ToString();
        }

        public static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                .Where(x => x % 2 == 0)
                .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                .ToArray();
        }

        public static byte[] Hash(string input)
        {
            using (var sha1 = new SHA1Managed())
            {
                return sha1.ComputeHash(Encoding.UTF8.GetBytes(input));
            }
        }
    }
}
