﻿using System;
using System.Linq;
using System.Collections.Generic;
using Live.Data.Interfaces;

namespace SgGeolocReports
{
    public class GeoFenceSearch : IGeoFenceSearch
    {
        private readonly IAddressMasterRepo _addrMasterRepo;
        private readonly IFetchPolicyFactory<IAddressMasterFetchPolicy> _addrMasterFpf;
        private readonly IGeoFenceLoader _gfl;

        public GeoFenceSearch(
            IAddressMasterRepo addrMasterRepo,
            IFetchPolicyFactory<IAddressMasterFetchPolicy> addrMasterFpf,
            IGeoFenceLoader gfl)
        {
            _addrMasterRepo = addrMasterRepo;
            _addrMasterFpf = addrMasterFpf;
            _gfl = gfl;
        }

        public IEnumerable<GeoFence> LoadGeoFences(string comCode, string reportName)
        {
            return _addrMasterRepo
                .Fetch(_addrMasterFpf.NewFetchPolicy()
                    .ForTransporterReport(comCode, reportName)
                    .IncludeGeoFence())
                .SelectMany(x => x.GeoFences)
                .ToList()
                .Where(x => string.Equals(x.GeoFenceType.Code, "BOUNDARY", StringComparison.OrdinalIgnoreCase))
                .Select(x => _gfl.Load(x.Id, x.Description, x.GeoFenceShapeType.Code, x.Details));
        }
    }
}
