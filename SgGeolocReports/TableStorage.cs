﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Threading.Tasks;

using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Serilog;

namespace SgGeolocReports
{
    public interface ITableStorage<out T>
    {
        IEnumerable<T> ExecuteQuery(string partitionKey, string rowKeyFrom, string rowKeyTo);
    }

    public class TableStorage<T> : ITableStorage<T> where T : ITableEntity, new()
    {
        private readonly object _locker = new object();

        private const string GeoLocationStorageConnectionStringKey = "GeoLocations";
        private readonly IEnvironmentSettings _environmentSettings;

        private CloudTable _table;
        private bool _initialised;

        private readonly ILogger _logger;

        public CloudTable Table
        {
            get
            {
                if (_initialised)
                    return _table;

                lock (_locker)
                {
                    if (_initialised)
                        return _table;

                    InitTable();
                    _initialised = true;
                }

                return _table;
            }
        }

        public TableStorage(IEnvironmentSettings environmentSettings, ILogger logger)
        {
            _environmentSettings = environmentSettings;
            _logger = logger;
        }

        private void InitTable()
        {
            _logger.Debug("Connecting to TableStorage");

            var storageConnectionString = _environmentSettings.GetConnectionString(GeoLocationStorageConnectionStringKey);
            if (string.IsNullOrWhiteSpace(storageConnectionString))
                throw new ConfigurationErrorsException("Can't get storage connection string: " + GeoLocationStorageConnectionStringKey);

            var attribute = (TableAttribute)Attribute.GetCustomAttribute(typeof(T), typeof(TableAttribute));
            var tableName = attribute == null ? typeof(T).Name : attribute.Name;

            var storageAccount = CloudStorageAccount.Parse(storageConnectionString);
            var tableClient = storageAccount.CreateCloudTableClient();
            _table = tableClient.GetTableReference(tableName);
            _table.CreateIfNotExists();

            _logger.Debug("Connected to TableStorage");

        }

        public async Task Insert(T entity)
        {
            if (Table != null)
            {
                var insertOperation = TableOperation.Insert(entity);
                await Table.ExecuteAsync(insertOperation);
            }
        }

        public IEnumerable<T> ExecuteQuery(string partitionKey, string rowKeyFrom, string rowKeyTo)
        {
            if (string.IsNullOrWhiteSpace(partitionKey))
            {
                throw new ArgumentNullException(nameof(partitionKey));
            }

            var filter =
                TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, partitionKey);

            if (!string.IsNullOrWhiteSpace(rowKeyTo))
            {
                filter = TableQuery.CombineFilters(
                    filter,
                    TableOperators.And,
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.LessThanOrEqual, rowKeyTo));
            }

            if (!string.IsNullOrWhiteSpace(rowKeyFrom))
            {
                filter = TableQuery.CombineFilters(
                    filter,
                    TableOperators.And,
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual, rowKeyFrom));
            }

            var query = new TableQuery<T>().Where(filter);

            var entities = Table.ExecuteQuery(query);

            return entities;
        }
    }
}