﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Live.Data.Interfaces;

namespace SgGeolocReports
{
    public interface ITripSearch
    {
        IEnumerable<ITrip> LoadTrips(string comCode, DateTimeOffset startTime, DateTimeOffset endTime);


    }
}
