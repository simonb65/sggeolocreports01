using System;
using System.ComponentModel.DataAnnotations.Schema;
using Live.Data.Interfaces;
using Microsoft.WindowsAzure.Storage.Table;
using Serilog;

namespace SgGeolocReports
{
    public interface IGeoLocationDetail
    {
        DateTimeOffset GeoTimestamp { get; }
        double Latitude { get; }
        double Longitude { get; }
        double? Speed { get; }
        double? Heading { get; }
        double? Altitude { get; }
        double? Accuracy { get;}
        double? AltitudeAccuracy { get; }
    }


    [Table("GeoLocations")]
    public class GeoLocationDetail : TableEntity, IGeoLocationDetail
    {
        [IgnoreProperty]
        public DateTimeOffset GeoTimestamp
        {
            get => new DateTimeOffset(GeoTimestampUtc).ToOffset(TimeSpan.FromTicks(GeoTimestampOffsetTicks));
            set
            {
                GeoTimestampUtc = value.UtcDateTime;
                GeoTimestampOffsetTicks = value.Offset.Ticks;
            }
        }

        public DateTime GeoTimestampUtc { get; set; }
        public long GeoTimestampOffsetTicks { get; set; }

        public int DriverId { get; set; }
        public int LoginDetailId { get; set; }
        public int VehicleId { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double? Speed { get; set; }
        public double? Heading { get; set; }
        public double? Altitude { get; set; }
        public double? Accuracy { get; set; }
        public double? AltitudeAccuracy { get; set; }
        public string JobAssignments { get; set; }
        public string DriverJobKeys { get; set; }
        public string BulkRunMovements { get; set; }
    }


    public class TripGeoLocations
    {
        public TripGeoLocations(ITrip trip)
        {
            Trip = trip;
        }

        public ITrip Trip { get; }
        public GeoLocationDetail[] GeoLocations { get; set; }
    }

    public interface IGeoLocTableStore : ITableStorage<GeoLocationDetail>
    {
    }

    public class GeoLocTableStore : TableStorage<GeoLocationDetail>, IGeoLocTableStore
    {
        public GeoLocTableStore(IEnvironmentSettings environmentSettings, ILogger logger) 
            : base(environmentSettings, logger)
        {
        }
    }
}