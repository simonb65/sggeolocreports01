﻿using System;
using System.Linq;
using Live.Data;
using Live.Data.Interfaces;

namespace SgGeolocReports
{
    public interface IAddressMasterFetchPolicy : IFetchPolicy<IAddressMaster>
    {
        IAddressMasterFetchPolicy ForTransporterReport(string comCode, string reportName);
        IAddressMasterFetchPolicy IncludeGeoFence();
    }

    public class AddressMasterFetchPolicy : FetchPolicyBase<AddressMaster, IAddressMaster>, IAddressMasterFetchPolicy
    {
        public IAddressMasterFetchPolicy ForTransporterReport(string comCode, string reportName)
        {
            And(am => am.AddressUsesLocal.Any(au => au.ParentLocal.CommunityCode == comCode && au.IsActive && au.BusinessName == reportName));
            return this;
        }

        public IAddressMasterFetchPolicy IncludeGeoFence()
        {
            Include(x => x.GeoFencesLocal);
            return this;
        }

}

public class AddressMasterFetchPolicyFactory : FetchPolicyFactory<AddressMasterFetchPolicy, IAddressMasterFetchPolicy>
    {
    }
}
