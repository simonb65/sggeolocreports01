﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SgGeolocReports
{
    public interface IFetchPolicyFactory<out T>
    {
        T NewFetchPolicy();
    }

    public class FetchPolicyFactory<T, I> : IFetchPolicyFactory<I>
        where T : I, new()
    {
        public I NewFetchPolicy() => new T();
    }
}
