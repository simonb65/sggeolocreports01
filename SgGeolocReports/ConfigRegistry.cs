﻿using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serilog;

namespace SgGeolocReports
{
    public class ConfigRegistry : Registry
    {
        public ConfigRegistry()
        {
            Scan(s =>
            {
                s.TheCallingAssembly();
                s.WithDefaultConventions();
                s.AddAllTypesOf(typeof(IFetchPolicyFactory<>));
            });

            For<ILogger>().Use(x => Log.ForContext(x.ParentType)).AlwaysUnique();
        }
    }
}
