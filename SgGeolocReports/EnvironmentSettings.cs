﻿using System.Configuration;

namespace SgGeolocReports
{
    public interface IEnvironmentSettings
    {
        string GetAppSetting(string key);
        string GetConnectionString(string name);
    }

    public class EnvironmentSettings : IEnvironmentSettings
    {
        public string GetAppSetting(string key) => ConfigurationManager.AppSettings[key];
        public string GetConnectionString(string name) => ConfigurationManager.ConnectionStrings[name]?.ConnectionString;

    }
}