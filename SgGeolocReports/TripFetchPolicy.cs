﻿using System;
using Live.Data;
using Live.Data.Interfaces;

namespace SgGeolocReports
{
    public interface ITripFetchPolicy : IFetchPolicy<ITrip>
    {
        ITripFetchPolicy ForTransporter(string comCode);
        ITripFetchPolicy DateTimeRange(DateTimeOffset start, DateTimeOffset end);
        ITripFetchPolicy IncludeVehicle();
    }

    public class TripFetchPolicy : FetchPolicyBase<Trip, ITrip>, ITripFetchPolicy
    {
        public ITripFetchPolicy ForTransporter(string comCode)
        {
            And(x => x.VehicleLocal.AccountLocal.CommunityCode == comCode);
            return this;
        }

        public ITripFetchPolicy DateTimeRange(DateTimeOffset start, DateTimeOffset end)
        {
            And(x => x.CreateDate >= start && x.CreateDate < end);
            return this;
        }

        public ITripFetchPolicy IncludeVehicle()
        {
            Include(x => x.VehicleLocal);
            return this;
        }
    }

    public class TripFetchPolicyFactory : FetchPolicyFactory<TripFetchPolicy, ITripFetchPolicy>
    {
    }
}
