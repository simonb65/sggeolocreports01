﻿using System;
using System.Collections.Generic;
using Live.Data.Interfaces;

namespace SgGeolocReports
{
    public class TripSearch : ITripSearch
    {
        private readonly ITripRepo _tripRepo;
        private readonly IFetchPolicyFactory<ITripFetchPolicy> _tripFpf;

        public TripSearch(
            ITripRepo tripRepo,
            IFetchPolicyFactory<ITripFetchPolicy> tripFpf)
        {
            _tripRepo = tripRepo;
            _tripFpf = tripFpf;
        }

        public IEnumerable<ITrip> LoadTrips(string comCode, DateTimeOffset startTime, DateTimeOffset endTime)
        {
            return _tripRepo.Fetch(_tripFpf.NewFetchPolicy()
                .ForTransporter(comCode)
                .IncludeVehicle()
                .DateTimeRange(startTime, endTime));
        }
    }
}
