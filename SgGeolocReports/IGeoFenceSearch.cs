﻿using System;
using System.Collections.Generic;

namespace SgGeolocReports
{
    public interface IGeoFenceSearch
    {
        IEnumerable<GeoFence> LoadGeoFences(string comCode, string reportName);
    }
}
