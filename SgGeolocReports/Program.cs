﻿using System;
using System.IO;
using Serilog;
using Serilog.Formatting.Display;
using StructureMap;

namespace SgGeolocReports
{
    class Program
    {
        static void Main(string[] args)
        {
            InitLogging();
            var cont = InitIoc();

            var rm = cont.GetInstance<ReportManager>();

            var now = DateTimeOffset.Now;

            using (var s = new StreamWriter($"SG56BC32-GeoFence-{now:yyyyMMdd}.csv"))
            {
                rm.Run("SG56BC32", "GFR01", now.AddDays(-5), now, s);
            }
        }

        public static void InitLogging()
        {
            var version = typeof(Program).Assembly.GetName().Version;

            Log.Logger = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .Enrich.WithProperty("Version", version)
                .Enrich.WithThreadId()
                .WriteTo.Logger(c => c
                    .WriteTo.Trace()
                    .MinimumLevel.Verbose())
                .WriteTo.Logger(c => c
                    .WriteTo.LiterateConsole()
                    .MinimumLevel.Information())
                .MinimumLevel.Verbose()
                .CreateLogger();

            Log.Information("{App} {Version} Started", "SG GeoLocation Reports", version);
            Log.Warning("DEBUG Build");
        }

        private static IContainer InitIoc()
        {
            return new Container(c =>
            {
                c.Scan(s =>
                {
                    s.AssembliesFromApplicationBaseDirectory();
                    s.TheCallingAssembly();
                    s.LookForRegistries();
                });
            });
        }
    }
}
