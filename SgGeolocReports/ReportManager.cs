﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Live.Data.Interfaces;
using Serilog;

namespace SgGeolocReports
{
    public class ReportManager
    {
        private readonly ITripSearch _tripSearch;
        private readonly IGeoFenceSearch _geoFenceSearch;
        private readonly IGeoLocTableStore _geoLocTableStore;
        private readonly IGeoFenceLoader _geoFenceLoader;
        private readonly ILogger _logger;

        public ReportManager(
            ITripSearch tripSearch,
            IGeoFenceSearch geoFenceSearch,
            IGeoLocTableStore geoLocTableStore,
            IGeoFenceLoader geoFenceLoader,
            ILogger logger)
        {
            _tripSearch = tripSearch;
            _geoFenceSearch = geoFenceSearch;
            _geoLocTableStore = geoLocTableStore;
            _geoFenceLoader = geoFenceLoader;
            _logger = logger;
        }

        class GeoLocVisit
        {
            public IVehicle Vehicle { get; set; }
            public GeoFence GeoFence { get; set; }

            public DateTimeOffset TimeIn { get; set; }
            public DateTimeOffset TimeOut { get; set; }
        }


        public void Run(string comCode, string reportName, DateTimeOffset startDate, DateTimeOffset endDate, StreamWriter s)
        {
            var geoFences = LoadGeoGences(comCode, reportName);
            var trips = LoadTrips(comCode, startDate, endDate);

            var tripIdx = 0;
            var geoLocCount = 0;

            var geoFenceVisits = new ConcurrentBag<GeoLocVisit>();


            using (_logger.BeginTimedOperation("Read GeoLocations from TableStorage", level: Serilog.Events.LogEventLevel.Verbose))
            {
                Parallel.ForEach(trips, t =>
                {
                    using (_logger.BeginTimedOperation($"Loading Trip - {t.Trip.Vehicle.VehicleRego}, {t.Trip.CreateDate:yyMMddHHmmss}", level: Serilog.Events.LogEventLevel.Verbose))
                    {
                        t.GeoLocations = _geoLocTableStore
                            .ExecuteQuery(
                                HashUtils.ByteArrayToString(t.Trip.PartitionKey),
                                startDate.UtcTicks.ToString(),
                                endDate.UtcTicks.ToString())
                            .ToArray();
                    }

                    _logger.Information(
                        "Loaded {TripCount}, {Vehicle}, {GeoLocs}, {TotalGeoLocs}",
                        Interlocked.Increment(ref tripIdx),
                        t.Trip.Vehicle.VehicleRego,
                        t.GeoLocations.Length,
                        Interlocked.Add(ref geoLocCount, t.GeoLocations.Length));

                    GeoFence curGf = null;
                    var entryTime = DateTimeOffset.MinValue;
                    foreach (var point in t.GeoLocations.OrderBy(x => x.Timestamp))
                    {
                        if (curGf == null)
                        {
                            curGf = geoFences.FirstOrDefault(x => x.ContainsPoint(point.Latitude, point.Longitude));
                            if (curGf == null)
                                continue;

                            entryTime = point.GeoTimestamp;
                        }

                        else
                        {
                            if (curGf.ContainsPoint(point.Latitude, point.Longitude))
                                continue;

                            var timeInGeoFence = point.GeoTimestamp - entryTime;

                            _logger.Debug("  {VehicleRego} in {GeoFence} => {GeoFenceTime}", t.Trip.Vehicle.VehicleRego, curGf.Name, timeInGeoFence);

                            geoFenceVisits.Add(new GeoLocVisit {Vehicle = t.Trip.Vehicle, GeoFence = curGf, TimeIn = entryTime, TimeOut = point.GeoTimestamp });

                            curGf = null;
                        }
                    }
                });
            }

            var sgTimezone = TimeSpan.FromHours(8);

            s.WriteLine("Account,VehicleNo,GeoFence,TimeIn,TimeOut,Duration");
            foreach (var glv in geoFenceVisits
                .OrderBy(x => x.Vehicle.VehicleRego)
                .ThenBy(x => x.GeoFence.Name)
                .ThenBy(x => x.TimeIn))
            {
                s.WriteLine($"{comCode},{glv.Vehicle.VehicleRego},{glv.GeoFence.Name},{glv.TimeIn.ToOffset(sgTimezone):yyyy-MM-dd HH:mm},{glv.TimeOut.ToOffset(sgTimezone): yyyy-MM-dd HH:mm},{glv.TimeOut - glv.TimeIn}");
            }

            _logger.Information("Done");

        }

        private IList<TripGeoLocations> LoadTrips(string comCode, DateTimeOffset startDate, DateTimeOffset endDate)
        {
            IList<TripGeoLocations> trips;
            using (_logger.BeginTimedOperation("Loading Trips", level: Serilog.Events.LogEventLevel.Verbose))
            {
                trips = _tripSearch
                    .LoadTrips(comCode, startDate, endDate)
                    .Select(x => new TripGeoLocations(x))
                    .ToList();
            }
            _logger.Information("Loaded {Trips} Trips", trips.Count);
            return trips;
        }

        private IList<GeoFence> LoadGeoGences(string comCode, string reportName)
        {
            IList<GeoFence> geoFences;
            using (_logger.BeginTimedOperation("Loading GeoFences", level: Serilog.Events.LogEventLevel.Verbose))
            {
                // var geoFenceJson = File.ReadAllText("GeoFences.json");
                // geoFences = _geoFenceLoader.LoadFromJson(geoFenceJson);

                geoFences = _geoFenceSearch.LoadGeoFences(comCode, reportName).ToList();
            }
            _logger.Information("Loaded {GeoFences} GeoFences", geoFences.Count);

            return geoFences;
        }
    }
}
