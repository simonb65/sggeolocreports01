using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SgGeolocReports
{
    public interface IGeoFenceLoader
    {
        GeoFence Load(int id, string name, string shapeType, string json);
    }

    public class GeoFenceLoader : IGeoFenceLoader
    {
        public GeoFence Load(int id, string name, string shapeType, string json) => 
            JsonConvert.DeserializeObject<GeoFence>(json, new GeoFenceConverter(id, name, shapeType));
    }

    public class GeoFenceConverter : JsonConverter
    {
        private readonly int _id;
        private readonly string _name;
        private readonly string _shapeType;

        public GeoFenceConverter(int id, string name, string shapeType)
        {
            _id = id;
            _name = name;
            _shapeType = shapeType;
        }

        public override bool CanConvert(Type objectType) => typeof(GeoFence).IsAssignableFrom(objectType);

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var item = JObject.Load(reader);

            if (string.Equals(_shapeType, "Polygon", StringComparison.OrdinalIgnoreCase))
            {
                var geoCoordsJson = item["LocationList"]?.Children()
                    .Select(x => new GeoCoordinate(
                            x["Latitude"]?.Value<double>() ?? 0.0, 
                            x["Longitude"]?.Value<double>() ?? 0.0, 
                            x["Ordinal"]?.Value<int>() ?? 0))
                    .ToArray();

                return new PolygonGeoFence(_id, _name, geoCoordsJson);
            }

            //if (string.Equals("Circle", gft, StringComparison.OrdinalIgnoreCase))
            //{
            //    var centreJson = item["Centre"];
            //    var centre = new GeoCoordinate(centreJson?["Latitude"]?.Value<double>() ?? 0.0, centreJson?["Longitude"]?.Value<double>() ?? 0.0);
            //    var radius = item["Radius"]?.Value<double>() ?? 0.0;

            //    return new CircleGeoFence(id, name, centre, radius);
            //}

            throw new ApplicationException("Unknown GeoFence ShapeType: " + (_shapeType ?? "Undefined"));
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) => throw new NotImplementedException();
    }
}